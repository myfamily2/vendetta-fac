-- Config file
require("config")

require("groups")
require("tweaks.vendetta-collisions")
require("tweaks.vendetta-long-reach")
require("tweaks.vendetta-stumps-be-gone")
require("tweaks.vendetta-stack-size")
require("tweaks.water-fix.data")

require("prototypes.military.ammobox.data")
require("prototypes.military.advanced-radar.data")
require("prototypes.military.machine-gun-turrets.data")
require("prototypes.resources.ingots.data")
require("prototypes.resources.ores.data")
require("prototypes.resources.ore-compress.data")
require("prototypes.resources.waterfill.data")
require("prototypes.logistic.flow-control.data")
require("prototypes.logistic.water-well.data")
require("prototypes.energy.nuclear-reactor.data")
require("prototypes.energy.wind-turbine.data")

if NEW_ICON_SET == true then
data.raw["technology"]["wind-turbine"]["icon"] = "__vendetta__/graphics/icons/wind-turbine.png"
data.raw["technology"]["wind-turbine"]["icon_size"] = 128
data.raw["technology"]["electronics"]["icon"] = "__vendetta__/graphics/icons/electronics.png"
data.raw["technology"]["electronics"]["icon_size"] = 128

data.raw["item-group"]["vendetta-group"]["icon"] = "__vendetta__/graphics/icons/vendetta-group.png"
data.raw["item-group"]["vendetta-group"]["icon_size"] = 64
data.raw["item-group"]["logistics"]["icon"] = "__vendetta__/graphics/icons/logistics.png"
data.raw["item-group"]["logistics"]["icon_size"] = 64
data.raw["item-group"]["combat"]["icon"] = "__vendetta__/graphics/icons/combat.png"
data.raw["item-group"]["combat"]["icon_size"] = 64
data.raw["item-group"]["production"]["icon"] = "__vendetta__/graphics/icons/production.png"
data.raw["item-group"]["production"]["icon_size"] = 64
data.raw["item-group"]["intermediate-products"]["icon"] = "__vendetta__/graphics/icons/intermediate-products.png"
data.raw["item-group"]["intermediate-products"]["icon_size"] = 64
end