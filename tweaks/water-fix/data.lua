data.raw.tile.water.variants.inner_corner = {
        picture = "__vendetta__/tweaks/water-fix/graphics/terrain/water/water-inner-corner.png",
        count = 6
      }
data.raw.tile.water.variants.outer_corner = {
        picture = "__vendetta__/tweaks/water-fix/graphics/terrain/water/water-outer-corner.png",
        count = 6
      }
data.raw.tile.water.variants.side = {
        picture = "__vendetta__/tweaks/water-fix/graphics/terrain/water/water-side.png",
        count = 8
      }
data.raw.tile.water.variants.u_transition = {
        picture = "__vendetta__/tweaks/water-fix/graphics/terrain/water/water-u.png",
        count = 6
      }
data.raw.tile.water.variants.o_transition = {
        picture = "__vendetta__/tweaks/water-fix/graphics/terrain/water/water-o.png",
        count = 6
      }
data.raw.tile.water.allowed_neighbors = nil -- any
data.raw.tile.water.layer = 80
data.raw.tile.deepwater.layer = 85
data.raw.tile.water.needs_correction = false
for _, item in pairs(data.raw.item) do 
	if item.name ~= "landfill" and item.place_as_tile then 
		item.place_as_tile.condition_size = 1
	end
end
