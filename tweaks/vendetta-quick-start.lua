QUICK_START ={
				{"iron-axe", 6},
				{"iron-plate", 592},
				{"copper-plate", 200},
				{"electronic-circuit", 200},
				{"iron-gear-wheel", 200},
				{"burner-inserter", 36},
				{"inserter", 50},
				{"transport-belt", 300},
				{"small-electric-pole", 50},
				{"medium-electric-pole", 20},
				{"big-electric-pole", 20},
				{"burner-mining-drill", 7},
				{"electric-mining-drill", 8},
				{"stone-furnace", 49},
				{"assembling-machine-1", 20},
				{"pipe", 100},
				{"wooden-chest", 20},
				{"steam-engine", 20},
				{"boiler", 28},
				{"offshore-pump", 2},
				{"pipe-to-ground", 20},
				{"lab", 1},
				{"train-stop", 20},
				{"rail", 900},
				{"diesel-locomotive", 10},
				{"cargo-wagon", 10},
				{"rail-signal", 50},
				{"rail-chain-signal", 20},
				{"small-lamp", 50},
				{"car", 1},
				{"basic-circuit-board", 200},
				{"burner-ore-crusher", 3},
				{"steel-plate", 100},
				{"firearm-magazine", 500},
			}

script.on_event(defines.events.on_player_created, function(event)
	local player = game.players[event.player_index]
	
	for _,item in pairs(QUICK_START) do
		if game.item_prototypes[item[1]] ~= nul then
			player.insert{name=item[1], count=item[2]}
		end
	end
end)
