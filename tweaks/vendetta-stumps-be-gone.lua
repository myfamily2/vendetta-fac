-- DO NOT set any of these values below 1; it WILL crash your game.
-- These values simply change the time that the stump lives on your map.  You will never see them.

data.raw["corpse"]["tree-01-stump"].time_before_removed = 1
data.raw["corpse"]["tree-02-stump"].time_before_removed = 1
data.raw["corpse"]["tree-03-stump"].time_before_removed = 1
data.raw["corpse"]["tree-04-stump"].time_before_removed = 1
data.raw["corpse"]["tree-05-stump"].time_before_removed = 1
data.raw["corpse"]["tree-06-stump"].time_before_removed = 1
data.raw["corpse"]["tree-07-stump"].time_before_removed = 1
data.raw["corpse"]["tree-08-stump"].time_before_removed = 1
data.raw["corpse"]["tree-09-stump"].time_before_removed = 1