data:extend(
{
	{
	type = "item-group",
	name = "vendetta-group",
	icon = "__vendetta__/graphics/vendetta-group-icon.png",
	icon_size = 128,
	inventory_order = "a",
	order = "d",
	},	
	{
	type = "item-subgroup",
	name = "vendetta-military-ammo",
	group = "vendetta-group",
	order = "a0",
	},
	{
	type = "item-subgroup",
	name = "vendetta-military-resources",
	group = "vendetta-group",
	order = "a1",
	},
	{
	type = "item-subgroup",
	name = "vendetta-military-flow",
	group = "vendetta-group",
	order = "a2",
	},
	{
	type = "item-subgroup",
	name = "vendetta-energy",
	group = "vendetta-group",
	order = "a3",
	},	
	{
	type = "item-subgroup",
	name = "vendetta-nuclear-products",
	group = "vendetta-group",
	order = "a4",
	},
	{
	type = "item-subgroup",
	name = "vendetta-terrain",
	group = "vendetta-group",
	order = "a5",
	},
	{
	type = "recipe-category",
	name = "vendetta-fission"
  	},
  	{
	type = "recipe-category",
	name = "vendetta-steaming"
  	},
  	{
	type = "recipe-category",
	name = "vendetta-water-cooling"
  	},
	{
	type = "recipe-category",
	name = "vendetta-water-well-production"
	}
}
)
