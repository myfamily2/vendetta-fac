-- Config file
	require("config")

-- Quick Start
	if QUICK_START_SET == true then
	require("tweaks.vendetta-quick-start")
	end

-- World map
	if WORLD_MAP_SET == true then
	require("tweaks.world-map.world-map-control")
	end

-- Flow Control
	require("prototypes.logistic.flow-control.control-flow")
  
-- Energy (Reactor, Wind turbine)
	require("prototypes.energy.control-energy")
  