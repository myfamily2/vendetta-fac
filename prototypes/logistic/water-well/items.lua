data:extend(
	{
		{
			type = "item",
			name = "water-well-pump",
			icon = "__vendetta__/prototypes/logistic/water-well/graphics/water-well-pump-icon.png",
			flags = {"goes-to-quickbar"},
			subgroup = "vendetta-military-flow",
			order = "w[water-well-pump]",
			place_result = "water-well-pump",
			stack_size = 20
		},
	}
)

