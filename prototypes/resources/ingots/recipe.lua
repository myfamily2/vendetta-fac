data:extend(
{
	{
    type = "recipe",
    name = "steel-ingot",
    enabled = false,
    energy_required = 15,
    ingredients = {{"iron-plate", 5}},
    result = "steel-ingot"
	},
	{
    type = "recipe",
    name = "lead-ingot",
	category = "smelting",
	enabled = false,
    energy_required = 5,
    ingredients = {{"lead-ore", 1}},
    result = "lead-ingot",
	result_count = 1
	},
	{
    type = "recipe",
    name = "tungsten-ingot",
	category = "smelting",
	enabled = false,
    energy_required = 5,
    ingredients = {{"tungsten-ore", 1}},
    result = "tungsten-ingot",
	result_count = 1
	},
	{
    type = "recipe",
    name = "uranium-ingot",
	category = "smelting",
	enabled = false,
    energy_required = 5,
    ingredients = {{"uranium-ore", 1}},
    result = "uranium-ingot",
	result_count = 1
	}
}
)