data:extend(
{  
	{
	type = "item",
    name = "steel-ingot",
    icon = "__vendetta__/prototypes/resources/ingots/steel-ingot-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-military-resources",
    order = "a0[steel-ingot]",
    stack_size = 100
	},
	{
    type = "item",
    name = "lead-ingot",
    icon = "__vendetta__/prototypes/resources/ingots/lead-ingot-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-military-resources",
    order = "a1[lead-ingot]",
    stack_size = 100
	},
	{
    type = "item",
    name = "tungsten-ingot",
    icon = "__vendetta__/prototypes/resources/ingots/tungsten-ingot-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-military-resources",
    order = "a2[tungsten-ingot]",
    stack_size = 100
	},
	{
    type = "item",
    name = "uranium-ingot",
    icon = "__vendetta__/prototypes/resources/ingots/uranium-ingot-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-military-resources",
    order = "a3[uranium-ingot]",
    stack_size = 100
	}
}
)
