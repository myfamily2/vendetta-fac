table.insert( data.raw["technology"]["steel-processing"].effects, { type = "unlock-recipe", recipe = "steel-ingot" } )

data:extend(
{
    {
    type = "technology",
    name = "non-ferrous-metals",
    icon = "__vendetta__/prototypes/resources/ingots/non-ferrous-metals-technology-icon.png",
	icon_size = 128,
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "lead-ingot"
      }
    },
    prerequisites = {"steel-processing"},
    unit =
    {
      count = 60,
      ingredients = {{"science-pack-1", 1}},
      time = 5
    },
    order = "c-b"
	},
	{
    type = "technology",
    name = "rare-metals",
    icon = "__vendetta__/prototypes/resources/ingots/rare-metals-technology-icon.png",
	icon_size = 128,
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "uranium-ingot"
      },
	  {
        type = "unlock-recipe",
        recipe = "tungsten-ingot"
      }
    },
	prerequisites = {"non-ferrous-metals"},
    unit =
    {
      count = 60,
      ingredients = 
      {
      {"science-pack-1", 1},
      {"science-pack-2", 1}
      },
      time = 5
    },
    order = "a0"
	}
}
)
