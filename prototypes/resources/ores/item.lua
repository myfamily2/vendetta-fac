data:extend(
{  
	{
    type = "item",
    name = "lead-ore",
    icon = "__vendetta__/prototypes/resources/ores/lead-ore-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "raw-resource",
    order = "ff[lead-ore]",
    stack_size = 50
	},
    {
    type = "item",
    name = "tungsten-ore",
    icon = "__vendetta__/prototypes/resources/ores/tungsten-ore-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "raw-resource",
    order = "ff[tungsten-ore]",
    stack_size = 50
	},
	{
    type = "item",
    name = "uranium-ore",
    icon = "__vendetta__/prototypes/resources/ores/uranium-ore-icon.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "raw-resource",
    order = "ff[uranium-ore]",
    stack_size = 50
	}
}
)
