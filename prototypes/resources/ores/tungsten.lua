tungsten = util.table.deepcopy(data.raw["resource"]["stone"])
tungsten.name = "tungsten-ore"
tungsten.icon = "__vendetta__/prototypes/resources/ores/tungsten-ore-icon.png"
tungsten.minable.result = "tungsten-ore"
tungsten.autoplace =
{
  control = "tungsten-ore",
  sharpness = 1,
  richness_multiplier = 1500,
  richness_multiplier_distance_bonus = 20,
  richness_base = 500,
  coverage = 0.015,
  peaks = {
    {
      noise_layer = "tungsten-ore",
      noise_octaves_difference = -1.5,
      noise_persistence = 0.3,
    },
  },
  starting_area_size = 600 * 0.015,
  starting_area_amount = 1500
}
tungsten.stages.sheet.filename = "__vendetta__/prototypes/resources/ores/tungsten-ore.png"
tungsten.map_color = {r=0.133, g=0.133, b=0.133}

data:extend({
  {
    type = "noise-layer",
    name = "tungsten-ore"
  },
  {
    type = "autoplace-control",
    name = "tungsten-ore",
    richness = true,
    order = "b-f"
  },
  tungsten
})
