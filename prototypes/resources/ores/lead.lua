lead = util.table.deepcopy(data.raw["resource"]["stone"])
lead.name = "lead-ore"
lead.icon = "__vendetta__/prototypes/resources/ores/lead-ore-icon.png"
lead.minable.result = "lead-ore"
lead.autoplace =
{
  control = "lead-ore",
  sharpness = 1,
  richness_multiplier = 1500,
  richness_multiplier_distance_bonus = 20,
  richness_base = 500,
  coverage = 0.015,
  peaks = {
    {
      noise_layer = "lead-ore",
      noise_octaves_difference = -1.5,
      noise_persistence = 0.3,
    },
  },
  starting_area_size = 600 * 0.015,
  starting_area_amount = 1500
}
lead.stages.sheet.filename = "__vendetta__/prototypes/resources/ores/lead-ore.png"
lead.map_color = {r=0.79, g=0.81, b=0.79}

data:extend({
  {
    type = "noise-layer",
    name = "lead-ore"
  },
  {
    type = "autoplace-control",
    name = "lead-ore",
    richness = true,
    order = "b-f"
  },
  lead
})
