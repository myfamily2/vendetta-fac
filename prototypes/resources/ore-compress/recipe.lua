data:extend({
	-- Iron ore compression
  {
    type = "recipe",
    name = "compress-iron-ore",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"iron-ore", 20}
    },
    result = "compressed-iron-ore"
  },

  {
    type = "recipe",
    name = "uncompress-iron-ore",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-iron-ore", 1}
    },
    result = "iron-ore",
    result_count = 20,
  },
  
  -- Copper ore compression
  {
    type = "recipe",
    name = "compress-copper-ore",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"copper-ore", 20}
    },
    result = "compressed-copper-ore"
  },
  
  {
    type = "recipe",
    name = "uncompress-copper-ore",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-copper-ore", 1}
    },
    result = "copper-ore",
    result_count = 20,
  },
  
  -- Coal compression
  {
    type = "recipe",
    name = "compress-coal",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"coal", 20},
    },
    result = "compressed-coal"
  },
  
  {
    type = "recipe",
    name = "uncompress-coal",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-coal", 1}
    },
    result = "coal",
    result_count = 20,
  },
  
  -- Stone compression
  {
    type = "recipe",
    name = "compress-stone",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"stone", 20},
    },
    result = "compressed-stone"
  },
  
  {
    type = "recipe",
    name = "uncompress-stone",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-stone", 1}
    },
    result = "stone",
    result_count = 20,
  },
  
  -- Uranium ore compression
  {
    type = "recipe",
    name = "compress-uranium-ore",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"uranium-ore", 20},
    },
    result = "compressed-uranium-ore"
  },
  
  {
    type = "recipe",
    name = "uncompress-uranium-ore",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-uranium-ore", 1}
    },
    result = "uranium-ore",
    result_count = 20,
  },
  
  -- Lead ore compression
  {
    type = "recipe",
    name = "compress-lead-ore",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"lead-ore", 20},
    },
    result = "compressed-lead-ore"
  },
  
  {
    type = "recipe",
    name = "uncompress-lead-ore",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-lead-ore", 1}
    },
    result = "lead-ore",
    result_count = 20,
  },
  
  -- Tungsten ore compression
  {
    type = "recipe",
    name = "compress-tungsten-ore",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = 
    {
      {"tungsten-ore", 20},
    },
    result = "compressed-tungsten-ore"
  },
  
  {
    type = "recipe",
    name = "uncompress-tungsten-ore",
    energy_required = 4,
    enabled = false,
    ingredients = 
    {
      {"compressed-tungsten-ore", 1}
    },
    result = "tungsten-ore",
    result_count = 20,
  },
  
  -- Iron plate compression
  {
    type = "recipe",
    name = "compress-iron",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = {{"iron-plate", 20}},
    result = "compressed-iron"
  },

  {
    type = "recipe",
    name = "decompress-iron",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = {{"compressed-iron", 1}},
    result = "iron-plate",
    result_count = 20
  },
  
  -- Copper compression
  {
    type = "recipe",
    name = "compress-copper",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = {{"copper-plate", 20}},
    result = "compressed-copper"
  },
  
  {
    type = "recipe",
    name = "decompress-copper",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = {{"compressed-copper", 1}},
    result = "copper-plate",
    result_count = 20
  },
  
  -- Steel compression
  {
    type = "recipe",
    name = "compress-steel",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = {{"steel-plate", 10}},
    result = "compressed-steel"
  },
  
  {
    type = "recipe",
    name = "decompress-steel",
    energy_required = 4,
    category = "advanced-crafting",
    enabled = false,
    ingredients = {{"compressed-steel", 1}},
    result = "steel-plate",
    result_count = 10
  },
  
})