data:extend({
	{
		type = "technology",
		name = "orecompresstech",
		icon = "__vendetta__/prototypes/resources/ore-compress/graphics/ore-compress.png",
		effects =
		{
			{
				type = "unlock-recipe",
				recipe = "compress-iron-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-iron-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-copper-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-copper-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-coal"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-coal"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-stone"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-stone"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-uranium-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-uranium-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-lead-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-lead-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-tungsten-ore"
			},
      {
				type = "unlock-recipe",
				recipe = "uncompress-tungsten-ore"
			}
		},
		prerequisites = {"automation-2","rare-metals"},
		unit =
		{
			count = 200,
			ingredients =
			{
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 20
		}
	},
  {
		type = "technology",
		name = "platecompresstech",
		icon = "__vendetta__/prototypes/resources/ore-compress/graphics/plate-compress.png",
    icon_size = 64,
		effects =
		{
			{
				type = "unlock-recipe",
				recipe = "compress-iron"
			},
      {
				type = "unlock-recipe",
				recipe = "decompress-iron"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-copper"
			},
      {
				type = "unlock-recipe",
				recipe = "decompress-copper"
			},
      {
				type = "unlock-recipe",
				recipe = "compress-steel"
			},
      {
				type = "unlock-recipe",
				recipe = "decompress-steel"
			}
		},
		prerequisites = {"automation-2","rare-metals"},
		unit =
		{
			count = 200,
			ingredients =
			{
				{"science-pack-1", 1},
				{"science-pack-2", 1}
			},
			time = 20
		}
	}
})
			