data:extend({

{
    type = "item",
    name = "advanced-radar",
    icon = "__vendetta__/prototypes/military/advanced-radar/graphics/item_icon_advanced_radar.png",
    flags = {"goes-to-quickbar"},
    subgroup = "defensive-structure",
    order = "z[radar]-a[radar]",
    place_result = "advanced-radar",
    stack_size = 10
  }
  
  })