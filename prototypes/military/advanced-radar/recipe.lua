data:extend({

{
    type = "recipe",
    name = "advanced-radar",
	enabled = false,
	energy_required = 5,
    ingredients =
    {
      {"electronic-circuit", 15},
      {"iron-gear-wheel", 5},
      {"iron-plate", 10}
    },
    result = "advanced-radar",
    requester_paste_multiplier = 4
  }
  
  })