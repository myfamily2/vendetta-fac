data:extend({

  {
    type = "radar",
    name = "advanced-radar",
    icon = "__vendetta__/prototypes/military/advanced-radar/graphics/radar_ssilk_4_icon.png",
    flags = {"placeable-player", "player-creation"},
    minable = {hardness = 0.2, mining_time = 0.5, result = "advanced-radar"},
    max_health = 150,
    corpse = "big-remnants",
    resistances =
    {
      {
        type = "fire",
        percent = 70
      }
    },
    collision_box = {{-1.4, -1.4}, {1.4, 1.4}},
    selection_box = {{-1.5, -1.5}, {1.5, 1.5}},
    energy_per_sector = "10MJ",
    max_distance_of_sector_revealed = 28,
    max_distance_of_nearby_sector_revealed = 6,
    energy_per_nearby_scan = "250kJ",
    energy_source =
    {
      type = "electric",
      usage_priority = "secondary-input"
    },
    energy_usage = "1200kW",
    pictures =
    {
      filename = "__vendetta__/prototypes/military/advanced-radar/graphics/radar_ssilk_4_sheet.png",
      priority = "low",
      width = 128,
      height = 128,
      apply_projection = false,
      direction_count = 64,
      line_length = 8,
      shift = {0.9, -1.0}
    },
    vehicle_impact_sound =  { filename = "__base__/sound/car-metal-impact.ogg", volume = 0.65 },
    working_sound =
    {
      sound = {
        {
          filename = "__base__/sound/radar.ogg"
        }
      },
      apparent_volume = 2,
    }
  }
  
  })