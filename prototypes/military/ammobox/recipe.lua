data:extend(
{
	{ -- Basic bullet
		type = "recipe",
		name = "firearm-magazine",
		energy_required = 2,
		ingredients = 
		{
			{"iron-plate", 2},
			{"lead-ingot", 1}
		},
		result = "firearm-magazine",
		result_count = 1
	},
    	{
        	type = "recipe",
        	name = "basic-bullet-ammo-box",
        	enabled = true,
       		energy_required = 12,
        	ingredients =
        	{
            		{"firearm-magazine", 100},
            		{"iron-plate", 20}
        	},
        	result = "basic-bullet-ammo-box",
        	result_count = 1
    	},
   	{
        	type = "recipe",
        	name = "basic-bullet-ammo-big-box",
        	enabled = true,
        	energy_required = 20,
        	ingredients =
        	{
            		{"basic-bullet-ammo-box", 5},
            		{"iron-plate", 40}
        	},
        	result = "basic-bullet-ammo-big-box",
        	result_count = 1
    	},
	{ -- Steel core bullet
		type = "recipe",
		name = "piercing-rounds-magazine",
		enabled = false,
		energy_required = 3,
		ingredients =
		{
			{"iron-plate", 2},
			{"lead-ingot", 1},
			{"steel-ingot", 1}
		},
		result = "piercing-rounds-magazine",
		result_count = 1
	},
	{
        	type = "recipe",
       		name = "armor-piercing-ammo-box-steel",
        	enabled = false,
        	energy_required = 12,
        	ingredients =
        	{
            		{"piercing-rounds-magazine", 100},
            		{"iron-plate", 20}
        	},
        	result = "armor-piercing-ammo-box-steel",
        	result_count = 1
    	},
    	{
        	type = "recipe",
        	name = "armor-piercing-ammo-big-box-steel",
        	enabled = false,
        	energy_required = 20,
        	ingredients =
        	{
            		{"armor-piercing-ammo-box-steel", 5},
            		{"iron-plate", 40}
        	},
       		result = "armor-piercing-ammo-big-box-steel",
        	result_count = 1
    	},
	{ -- Tungsten core bullet
		type = "recipe",
		name = "armor-piercing-ammo-tungsten",
		enabled = false,
		energy_required = 3,
		ingredients =
		{
			{"iron-plate", 2},
			{"lead-ingot", 1},
			{"tungsten-ingot", 1}
		},
		result = "armor-piercing-ammo-tungsten",
		result_count = 1
	},
	{
        	type = "recipe",
       		name = "armor-piercing-ammo-box-tungsten",
        	enabled = false,
        	energy_required = 12,
        	ingredients =
        	{
            		{"armor-piercing-ammo-tungsten", 100},
            		{"iron-plate", 20}
        	},
        	result = "armor-piercing-ammo-box-tungsten",
        	result_count = 1
    	},
    	{
        	type = "recipe",
        	name = "armor-piercing-ammo-big-box-tungsten",
        	enabled = false,
        	energy_required = 20,
        	ingredients =
        	{
            		{"armor-piercing-ammo-box-tungsten", 5},
            		{"iron-plate", 40}
        	},
       		result = "armor-piercing-ammo-big-box-tungsten",
        	result_count = 1
    	},
	{ -- Uranium core bullet
		type = "recipe",
		name = "armor-piercing-ammo-uranium",
		enabled = false,
		energy_required = 3,
		ingredients =
		{
			{"iron-plate", 2},
			{"lead-ingot", 1},
			{"uranium-ingot", 1}
		},
		result = "armor-piercing-ammo-uranium",
		result_count = 1
	},
	{
        	type = "recipe",
       		name = "armor-piercing-ammo-box-uranium",
        	enabled = false,
        	energy_required = 12,
        	ingredients =
        	{
            		{"armor-piercing-ammo-uranium", 100},
            		{"iron-plate", 20}
        	},
        	result = "armor-piercing-ammo-box-uranium",
        	result_count = 1
    	},
    	{
        	type = "recipe",
        	name = "armor-piercing-ammo-big-box-uranium",
        	enabled = false,
        	energy_required = 20,
        	ingredients =
        	{
            		{"armor-piercing-ammo-box-uranium", 5},
            		{"iron-plate", 40}
        	},
       		result = "armor-piercing-ammo-big-box-uranium",
        	result_count = 1
    	}
}
)
