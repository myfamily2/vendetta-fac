data:extend(
{
	{
        type = "ammo",
        name = "basic-bullet-ammo-box",
        icon = "__vendetta__/prototypes/military/ammobox/basic-ammo-box-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
                                {
                                    type = "damage",
                                    damage = { amount = 2 , type = "physical"}
                                }
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 1000,
        subgroup = "vendetta-military-ammo",
        order = "a1",
        stack_size = 10
	},
	{
        type = "ammo",
        name = "basic-bullet-ammo-big-box",
        icon = "__vendetta__/prototypes/military/ammobox/basic-ammo-big-box-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
                                {
                                    type = "damage",
                                    damage = { amount = 2 , type = "physical"}
                                }
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 5000,
        subgroup = "vendetta-military-ammo",
        order = "a2",
        stack_size = 5
    },
	{
        type = "ammo",
        name = "armor-piercing-ammo-box-steel",
        icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-box-steel-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
                                {
                                    type = "damage",
                                    damage = { amount = 5 , type = "physical"}
                                }
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 1000,
        subgroup = "vendetta-military-ammo",
        order = "a4",
        stack_size = 10
    },
	{
        type = "ammo",
        name = "armor-piercing-ammo-big-box-steel",
        icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-big-box-steel-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
                                {
                                    type = "damage",
                                    damage = { amount = 5 , type = "physical"}
                                }
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 5000,
        subgroup = "vendetta-military-ammo",
        order = "a5",
        stack_size = 5
    },
	{
    	type = "ammo",
    	name = "armor-piercing-ammo-tungsten",
    	icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-tungsten-icon.png",
    	flags = {"goes-to-main-inventory"},
    	ammo_type =
    	{
      		category = "bullet",
      		action =
      		{
        		type = "direct",
        		action_delivery =
        		{
          			type = "instant",
          			source_effects =
          				{
              				type = "create-explosion",
              				entity_name = "explosion-gunshot"
          				},
          			target_effects =
          				{
            				{
              					type = "create-entity",
              					entity_name = "explosion-hit"
            				},
            				{
              					type = "damage",
              					damage = { amount = 8 , type = "impact"}
            				}
          				}
        		}
      		}
    	},
    	magazine_size = 10,
    	subgroup = "vendetta-military-ammo",
    	order = "a6",
    	stack_size = 100
	},
	{
        type = "ammo",
        name = "armor-piercing-ammo-box-tungsten",
        icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-box-tungsten-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
                                {
                                    type = "damage",
                                    damage = { amount = 8 , type = "physical"}
                                }
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 1000,
        subgroup = "vendetta-military-ammo",
        order = "a7",
        stack_size = 10
    },
	{
        type = "ammo",
        name = "armor-piercing-ammo-big-box-tungsten",
        icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-big-box-tungsten-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
                                {
                                    type = "damage",
                                    damage = { amount = 8 , type = "physical"}
                                }
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 5000,
        subgroup = "vendetta-military-ammo",
        order = "a8",
        stack_size = 5
    },
	{
    	type = "ammo",
    	name = "armor-piercing-ammo-uranium",
    	icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-uranium-icon.png",
    	flags = {"goes-to-main-inventory"},
    	ammo_type =
    	{
      		category = "bullet",
      		action =
      		{
        		type = "direct",
        		action_delivery =
        		{
          			type = "instant",
          			source_effects =
          				{
              				type = "create-explosion",
              				entity_name = "explosion-gunshot"
          				},
          			target_effects =
          				{
            				{
              					type = "create-entity",
              					entity_name = "explosion-hit"
            				},
            				{
              					type = "damage",
              					damage = { amount = 9 , type = "impact"}
            				},
           					{
              					type = "damage",
              					damage = { amount = 3 , type = "fire"}
            				}
          				}
        		}
      		}
    	},
    	magazine_size = 10,
    	subgroup = "vendetta-military-ammo",
    	order = "a9",
    	stack_size = 100
	},
	{
        type = "ammo",
        name = "armor-piercing-ammo-box-uranium",
        icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-box-uranium-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
            					{
              						type = "damage",
              						damage = { amount = 9 , type = "impact"}
            					},
           						{
              						type = "damage",
              						damage = { amount = 3 , type = "fire"}
            					}
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 1000,
        subgroup = "vendetta-military-ammo",
        order = "aa1",
        stack_size = 10
    },
	{
        type = "ammo",
        name = "armor-piercing-ammo-big-box-uranium",
        icon = "__vendetta__/prototypes/military/ammobox/armor-piercing-ammo-big-box-uranium-icon.png",
        flags = {"goes-to-main-inventory"},
        ammo_type =
        {
            category = "bullet",
            action =
            {
                {
                    type = "direct",
                    action_delivery =
                    {
                        {
                            type = "instant",
                            source_effects =
                            {
                                {
                                    type = "create-explosion",
                                    entity_name = "explosion-gunshot"
                                }
                            },
                            target_effects =
                            {
                                {
                                    type = "create-entity",
                                    entity_name = "explosion-hit"
                                },
            					{
              						type = "damage",
              						damage = { amount = 9 , type = "impact"}
            					},
           						{
              						type = "damage",
              						damage = { amount = 3 , type = "fire"}
            					}
                            }
                        }
                    }
                }
            }
        },
        magazine_size = 5000,
        subgroup = "vendetta-military-ammo",
        order = "aa2",
        stack_size = 5
    }
}
)
