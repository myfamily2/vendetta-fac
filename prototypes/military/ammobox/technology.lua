data:extend(
{
  {
    type = "technology",
    name = "military-2",
    icon = "__base__/graphics/technology/military.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "piercing-rounds-magazine"
      },
	  {
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-box-steel"
      },
	  {
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-big-box-steel"
      },
      {
        type = "unlock-recipe",
        recipe = "grenade"
      }
    },
    prerequisites = {"military", "steel-processing"},
    unit =
    {
      count = 20,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 15
    },
    order = "e-a-b"
  },
    {
    type = "technology",
    name = "military-3",
    icon = "__base__/graphics/technology/military.png",
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "poison-capsule"
      },
      {
        type = "unlock-recipe",
        recipe = "slowdown-capsule"
      },
      {
        type = "unlock-recipe",
        recipe = "combat-shotgun"
      },
      	{
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-tungsten"
      	},
	{
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-box-tungsten"
      	},
	{
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-big-box-tungsten"
      	},
      	{
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-uranium"
      	},
	{
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-box-uranium"
      	},
	{
        type = "unlock-recipe",
        recipe = "armor-piercing-ammo-big-box-uranium"
      	}
    },
    prerequisites = {"military-2", "laser", "rocketry", "rare-metals"},
    unit =
    {
      count = 50,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 2},
        {"science-pack-3", 1}
      },
      time = 30
    },
    order = "e-a-c"
  }
}
)
