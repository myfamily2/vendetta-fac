data:extend({

	{
		type = "recipe",
		name = "heavy-machine-gun-turret-mk1-recipe",
		enabled = "true",
		energy_required = 4,
		ingredients =
		{			
			{"gun-turret", 2},
			{"steel-plate", 2},
			{"electronic-circuit", 2}
		},
		result = "heavy-machine-gun-turret-mk1",
		order="t-a",
	}
  
})