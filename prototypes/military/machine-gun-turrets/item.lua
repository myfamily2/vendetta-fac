data:extend({

	{
		type = "item",
		name = "heavy-machine-gun-turret-mk1",
		icon = "__vendetta__/prototypes/military/machine-gun-turrets/graphics/heavy-machine-gun-turret-mk1_icon.png",
		flags = {"goes-to-quickbar"},
		subgroup = "vendetta-military-ammo",
		order = "b1",
		place_result = "heavy-machine-gun-turret-mk1",
		stack_size = 50, 
		default_request_amount = 10,
	}
  
})