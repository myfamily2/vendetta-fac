turret_gun2f12 =
	{
		filename = "__vendetta__/prototypes/military/machine-gun-turrets/graphics/heavy-machine-gun-turret-mk1_place.png",
		priority = "medium",
		width = 128,
		height = 128,
		direction_count = 8,
		frame_count = 1,
		axially_symmetrical = false,
		shift = {0.25, -0.94},
	}

data:extend({

	{
		type = "ammo-turret",
		name = "heavy-machine-gun-turret-mk1",
		icon = "__vendetta__/prototypes/military/machine-gun-turrets/graphics/heavy-machine-gun-turret-mk1_icon.png",
		flags = {"placeable-player", "player-creation"},
		minable = {mining_time = 0.5, result = "heavy-machine-gun-turret-mk1"},
		max_health = 600,
		resistances =
		{
			{type = "physical", decrease = 4, percent = 70,},
			{type = "explosion", decrease = 4, percent = 70,},
			{type = "fire", percent = 50,},
		},						
		corpse = "small-remnants",
		collision_box = {{-0.7, -0.7}, {0.7, 0.7}},
		selection_box = {{-1.0, -1.0}, {1.0, 1.0}},
		rotation_speed = 0.020, -- 0.015 orginal
		preparing_speed = 0.08,
		folding_speed = 0.08,
		dying_explosion = "medium-explosion",
		inventory_size = 3,
		automated_ammo_count = 30,
							
		folded_animation = (function()
		local res = util.table.deepcopy(turret_gun2f12)
		res.frame_count = 1
		res.line_length = 1
		return res
		end)(),
		preparing_animation = turret_gun2f12,
			
		prepared_animation =
		{
			filename = "__vendetta__/prototypes/military/machine-gun-turrets/graphics/heavy-machine-gun-turret-mk1_sheet.png",
			priority = "medium",
			width = 128,
			height = 128,
			direction_count = 64,
			frame_count = 1,
			line_length = 8,
			axially_symmetrical = false,
			shift = {0.25, -0.94},			
		},
		
		folding_animation = (function()
		local res = util.table.deepcopy(turret_gun2f12)
		res.run_mode = "backward"
		return res
		end)(),
			
		attack_parameters =
		{
			type="projectile",
			ammo_category = "bullet",
			cooldown = 8,
			projectile_center = {0, 0},
			projectile_creation_distance = 2.0,
			damage_modifier = 1.8,
			shell_particle = 
			{
				name = "shell-particle",
				direction_deviation = 0.1,
				speed = 0.1,
				speed_deviation = 0.03,
				center = {0, 0}, -- {0, 0.6},
				creation_distance = 0.6,
				starting_frame_speed = 0.2,
				starting_frame_speed_deviation = 0.1
			},
			range = 28,
			sound =
			{
				{
					filename = "__base__/sound/railgun.ogg",
					volume = 0.3
				}
			}
		},
		call_for_help_radius = 40
	}
  
})