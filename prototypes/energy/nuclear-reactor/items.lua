data:extend(
{
  {
    type = "item",
    name = "nuclear-reactor", -- Ядерный реактор
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/nuclear-reactor.png",
    flags = {"goes-to-quickbar"},
    subgroup = "vendetta-energy",
    order = "a0",
    place_result = "nuclear-reactor",
    stack_size = 10
  },
  {
    type = "item",
    name = "reactor-interface", -- Интерфейс реактора
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/entity/nuclear-reactor/nuclear-reactor-interface.png",
    flags = {"goes-to-quickbar"},
    subgroup = "vendetta-energy",
    order = "a0-b",
    place_result = "reactor-interface",
    stack_size = 10
  },
  {
    type = "item",
    name = "cooling-tower", -- Градирня
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/cooling-tower.png",
    flags = {"goes-to-quickbar"},
    subgroup = "vendetta-energy",
    order = "a1",
    place_result = "cooling-tower",
    stack_size = 10
  },
  {
    type = "item",
    name = "steam-turbine", -- Паровая турбина
    icons =
    {
      {
        icon = "__base__/graphics/icons/steam-engine.png",
      },
      {
        icon = "__base__/graphics/icons/steam-engine.png",
        tint = turbine_tint
      }
    },
    flags = {"goes-to-quickbar"},
    subgroup = "vendetta-energy",
    order = "a2",
    place_result = "steam-turbine",
    stack_size = 10
  },
  {
    type = "item",
    name = "peak-turbine",
    icons =
    {
      {
        icon = "__base__/graphics/icons/steam-engine.png",
      },
      {
        icon = "__base__/graphics/icons/steam-engine.png",
        tint = peak_turbine_tint
      }
    },
    flags = {"goes-to-quickbar"},
    subgroup = "vendetta-energy",
    order = "a3",
    place_result = "peak-turbine",
    stack_size = 10
  },
  {
    type = "item",
    name = "enriched-fuel", -- Обогащенное топливо
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/enriched-fuel.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-nuclear-products",
    order = "b[enriched-fuel]",
    stack_size = 10
  },
  {
    type = "item",
    name = "plutonium", -- Плутоний
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/plutonium.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-nuclear-products",
    order = "c[plutonium]",
    stack_size = 10
  },
  {
    type = "item",
    name = "spent-fuel", -- Отработанное топливо
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/spent-fuel.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-nuclear-products",
    order = "d[spent-fuel]",
    stack_size = 100
  },
  {
    type = "item",
    name = "depleted-uranium", -- Обедненный уран
    icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/depleted-uranium.png",
    flags = {"goes-to-main-inventory"},
    subgroup = "vendetta-nuclear-products",
    order = "e[depleted-uranium]",
    stack_size = 100
  }
})
