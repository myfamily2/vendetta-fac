turbine_tint = { r = 0.125, g = 0.25, b = 0.5, a = 0.333 }
peak_turbine_tint = { r = 0.125, g = 0.5, b = 0.25, a = 0.333 }

require("prototypes.energy.nuclear-reactor.entities")
require("prototypes.energy.nuclear-reactor.items")
require("prototypes.energy.nuclear-reactor.fluid")
require("prototypes.energy.nuclear-reactor.recipes")
require("prototypes.energy.nuclear-reactor.signals")
require("prototypes.energy.nuclear-reactor.technology")
