data:extend(
{
-- Building recipes --
-- (Реактор)
  	{
    	type = "recipe",
    	name = "nuclear-reactor",
    	enabled = false,
    	energy_required = 20,
    	ingredients =
    		{
      		{"concrete", 50},
      		{"steel-plate", 20},
      		{"pipe", 25},
      		{"express-pump", 4},
      		{"advanced-circuit", 10}
    		},
    	result = "nuclear-reactor"
  	},
-- (Градирня)
  	{
    	type = "recipe",
    	name = "cooling-tower",
    	enabled = false,
    	energy_required = 20,
    	ingredients =
    		{
      		{"concrete", 20},
      		{"steel-plate", 10},
      		{"pipe", 10},
      		{"express-pump", 4}
    		},
    	result = "cooling-tower"
  	},
-- (Паровая турбина)
  	{
    	type = "recipe",
   		name = "steam-turbine",
    	enabled = false,
    	energy_required = 20,
    	ingredients =
    		{
      		{"steam-engine", 25}
    		},
    	result = "steam-turbine"
  	},
-- (Паровая турбина пиковой нагрузки)
  	{
    	type = "recipe",
    	name = "peak-turbine",
    	enabled = false,
    	energy_required = 20,
    	ingredients =
    		{
      		{"steam-engine", 25}
    		},
    	result = "peak-turbine"
  	},  
-- Cooling tower water cooling recipe
-- (Охлаждение воды в градирне)
  	{
    	type = "recipe",
    	name = "water-cooling",
    	category = "vendetta-water-cooling",
    	enabled = true,
    	hidden = true,
    	energy_required = 0.2,
    	ingredients =
    		{
      		{type="fluid", name="water", amount=30}
    		},
    	results =
    		{
      		{type="fluid", name="water", amount=25}
    		},
    	icon = "__base__/graphics/icons/fluid/water.png",
    	subgroup = "fluid-recipes",
    	order = "z"
  	},
-- Cooling tower steam dummy recipe
  	{
    	type = "recipe",
    	name = "cooling-tower-steam",
    	category = "vendetta-steaming",
    	enabled = true,
    	hidden = true,
    	energy_required = 600,
    	ingredients =
    		{
      		{type="fluid", name="water", amount=0.1}
    		},
    	results =
    		{
      		{type="fluid", name="water", amount=0}
    		},
    	icon = "__base__/graphics/icons/fluid/water.png",
    	subgroup = "fluid-recipes",
    	order = "z"
  	},
-- Fuel processing
-- (Урановый раствор)
  	{
    	type = "recipe",
    	name = "uranium-slurry",
    	icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/uranium-slurry.png",
    	subgroup = "vendetta-nuclear-products",
    	order = "a0",
    	category = "crafting-with-fluid",
    	enabled = false,
    	energy_required = 5,
    	ingredients =
    		{
      		{type="fluid", name="sulfuric-acid", amount=1},
      		{"uranium-ore", 8}
    		},
    	results =
    		{
      		{type="fluid", name="uranium-slurry", amount=8}
    		}
  	},
-- Fuel enrichment
-- (Разделение урана)
  	{
    	type = "recipe",
    	name = "uranium-separation",
    	icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/uranium-separation.png",
    	subgroup = "vendetta-nuclear-products",
    	order = "a1",
    	category = "crafting-with-fluid",
    	enabled = false,
    	energy_required = 30,
    	ingredients =
    		{
      		{type="fluid", name="uranium-slurry", amount=8}
    		},
    	results =
    		{
      		{"enriched-fuel", 1},
      		{"depleted-uranium", 7}
    		}
  	},
-- (МОКС-топливо)
  	{
    	type = "recipe",
    	name = "mox-fuel",
    	icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/mox.png",
    	subgroup = "vendetta-nuclear-products",
    	order = "a2",
    	category = "advanced-crafting",
    	enabled = false,
    	energy_required = 5,
    	ingredients =
    		{
      		{"depleted-uranium", 3},
      		{"plutonium", 1}
    		},
    	results =
    		{
      		{"enriched-fuel", 4}
    		}
  	},  
-- Waste processing
-- (Переработка топлива)
  	{
    	type = "recipe",
    	name = "fuel-reprocessing",
    	icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/fuel-reprocessing.png",
    	subgroup = "vendetta-nuclear-products",
    	order = "a3",
		category = "crafting-with-fluid",
    	enabled = false,
    	energy_required = 20,
    	ingredients =
    		{
      		{type="fluid", name="sulfuric-acid", amount=0.5},
      		{"spent-fuel", 8}
    		},
    	results =
    		{
      		{"plutonium", 1},
      		{type="fluid", name="uranium-slurry", amount=4}
    		}
  	},  
-- Reactor fuel cycles
  	{
    	type = "recipe",
    	name = "fission-reaction",
    	icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/fission-reaction.png",
    	order = "a4",
    	subgroup = "vendetta-nuclear-products",
    	category = "vendetta-fission",
    	enabled = false,
    	energy_required = 50,
    	ingredients =
    		{
      		{"enriched-fuel", 1}
    		},
    	results =
    		{
      		{"spent-fuel", 1}
    		}
  	},
  	{
    	type = "recipe",
    	name = "breeder-reaction",
    	icon = "__vendetta__/prototypes/energy/nuclear-reactor/graphics/icons/breeder-reaction.png",
    	order = "a5",
    	subgroup = "vendetta-nuclear-products",
   		category = "vendetta-fission",
    	enabled = false,
    	energy_required = 50,
    	ingredients =
    		{
      		{"enriched-fuel", 1},
      		{"depleted-uranium", 7}
    		},
    	results =
    		{
      		{"plutonium", 1},
      		{"spent-fuel", 7}
    		}
  	}
})
