data:extend(
{
	{
        type = "recipe",
        name = "wind-turbine",
        energy_required = 5,
        enabled = "false",
        ingredients = {
            {"electronic-circuit", 4},
            {"electric-engine-unit", 4},
            {"iron-gear-wheel", 8},
            {"steel-plate", 20}
        },
        result = "wind-turbine"
	},
	{
	type = "recipe",
	name = "wind-turbine-low",
	energy_required = 5,
	enabled = "false",
	ingredients = {
            {"electronic-circuit", 2},
            {"electric-engine-unit", 1},
            {"iron-gear-wheel", 8},
            {"steel-plate", 10}
	},
	result = "wind-turbine-low"
	}
}
)
