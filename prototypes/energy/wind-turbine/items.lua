data:extend(
{
	{
	type = "item",
	name = "wind-turbine",
	icon = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_icon.png",
	flags = {"goes-to-quickbar"},
	subgroup = "vendetta-energy",
	order = "a5",
	place_result = "wind-turbine",
	stack_size = 10
	},
	{
	type = "item",
	name = "wind-turbine-low",
	icon = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_low_icon.png",
	flags = {"goes-to-quickbar"},
	subgroup = "vendetta-energy",	
	order = "a4",		
	place_result = "wind-turbine-low",
	stack_size = 10
	}
}
)
