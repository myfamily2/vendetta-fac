data:extend({
    {
		type = "generator",
		name = "wind-turbine",
		icon = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_icon.png",
		flags = {"placeable-neutral","player-creation"},
		minable = {hardness = 0.2, mining_time = 0.5, result = "wind-turbine"},
		max_health = 50,
		corpse = "medium-remnants",
		effectivity = 1,
		fluid_usage_per_tick = 0.0286,
		production = "150kW",
		resistances = {
			{
				type = "physical",
				percent = 10
			}
		},
		collision_box = {{-1, -1}, {1, 1}},
		selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
		fluid_box = {
			base_area = 1,
			pipe_connections = { }
		},
		energy_source = {
			type = "electric",
			usage_priority = "primary-output"
		},
		horizontal_animation = {
			filename = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_horizontal_sheet.png",
			width = 300,
			height = 175,
			frame_count = 20,
			line_length = 5,
			shift = {2.48, -1.45}
		},
		vertical_animation = {
			filename = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_vertical_sheet.png",
			width = 300,
			height = 175,
			frame_count = 20,
			line_length = 5,
			shift = {2.48, -1.45}
		},
        working_sound = {
            sound = {
                filename = "__base__/sound/train-wheels.ogg",
                volume = 0.6
            },
            match_speed_to_activity = true,
        },
        min_perceived_performance = 0.25,
        performance_to_sound_speedup = 0.2
    },
    {
		type = "generator",
		name = "wind-turbine-low",
		icon = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_low_icon.png",
		flags = {"placeable-neutral","player-creation"},
		minable = {mining_time = 1, result = "wind-turbine-low"},
		max_health = 50,
		corpse = "big-remnants",
		effectivity = 1,
		fluid_usage_per_tick = 0.010,
		resistances =
		{
			{
				type = "physical",
				percent = 10
			}
		},
		collision_box = {{-1, -1}, {1, 1}},
		selection_box = {{-0.5, -0.5}, {0.5, 0.5}},
		fluid_box =
		{
			base_area = 1,			
			pipe_connections = { }
			
		},
		energy_source =
		{
			type = "electric",
			usage_priority = "primary-output"
		},
		horizontal_animation =
		{
			filename = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_low_sheet.png",
			width = 175,
			height = 175,
			frame_count = 20,
			line_length = 5,
			shift = {1.7,-1.4}
		},
		vertical_animation =
		{
			filename = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_low_sheet.png",
			width = 175,
			height = 175,
			frame_count = 20,
			line_length = 5,
			shift = {1.7,-1.4}
		},		
		smoke =
		{
			
		},
	working_sound =
    {
      sound =
      {
        filename = "__base__/sound/train-wheels.ogg",
        volume = 0.6
      },
      match_speed_to_activity = true,
    },
    min_perceived_performance = 0.25,
    performance_to_sound_speedup = 0.2
  }
}
)
