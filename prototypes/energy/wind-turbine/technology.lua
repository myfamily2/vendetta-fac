data:extend(
{
	{
        type = "technology",
        name = "wind-turbine",
        icon = "__vendetta__/prototypes/energy/wind-turbine/graphics/wind_turbine_technology.png",
        icon_size = 128,
        prerequisites = {"electric-engine"},
        effects = {
            {
                type = "unlock-recipe",
                recipe = "wind-turbine"
            },
            {
                type = "unlock-recipe",
                recipe = "wind-turbine-low"
            }
        },
        unit = {
            count = 150,
            ingredients = {{"science-pack-1", 1}, {"science-pack-2", 1}},
            time = 15
        }
	}
}
)

