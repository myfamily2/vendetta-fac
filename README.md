# Vendetta Mod Pack #

Vanilla Factorio extended.

### version 0.3.0 ###

* [Ore and plate compression technology](https://forums.factorio.com/viewtopic.php?f=93&t=41280) integrated & rebalanced
* New icon set added for groups & technologies
* [AdvancedRadar](https://mods.factorio.com/mod/AdvancedRadar) v.0.0.1 integrated & modified. (Radar graphics by YuokiTani)
* New machine gun turret added. (Radar graphics by YuokiTani)

### version 0.2.3 ###

* Reactors & Wind turbine dual fix

### version 0.2.2 ###

* Factorio World generation & Oil Patches Organizer dual fix
* Stack size rebalanced

### version 0.2.1 ###

* [Oil Patches Organizer](https://mods.factorio.com/mods/Apriori/Oil%20Patches%20Organizer) v.0.0.2 integrated
* [Water Fix](https://mods.factorio.com/mods/Earendel/water-fix) v.0.1.1 integrated
* Waterfill added
* Wind Turbine some fix

### version 0.1.1 ###

* [Long Reach](https://github.com/jaguilar/long-reach) v.0.0.6 integrated
* [Squeak Through](https://github.com/Suprcheese/Squeak-Through) v.1.1.6 integrated
* [Stumps Be Gone](https://mods.factorio.com/mods/wormmus/stumps-be-gone) v.0.1.1 integrated
* [Flow Control](https://mods.factorio.com/mods/GotLag/Flow%20Control) v.2.1.4 integrated
* [Reactors](https://mods.factorio.com/mods/GotLag/Reactors) v.1.6.0 integrated & rebalanced
* [Factorio World](https://github.com/TheOddler/FactorioWorld) v.0.2.1 integrated
* [Water Well](https://mods.factorio.com/mods/binbinhfr/WaterWell) v.1.0.15 integrated
* [Wind Turbine](https://mods.factorio.com/mods/__init__/Wind_Turbine) v.0.3.0 integrated & rebalanced
* Ammobox & big Ammobox added
* Steel, lead, tungsten, uranium ingots & technology added
* Lead, tungsten, uranium ores added
* Quick start capability added

